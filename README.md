--- **Description** ---

This is a single-page application that lists SpaceX past and upcoming rocket launches; 
Mission title, launch site, date of launch and type of rocket used. 
Every entry of the "past launches"-category has a built in link that leads the user to a respective wikipedia-page, in which the user can read further on the project.

-- **Pre-requisites** --

-   NodeJS

-- **Install** --

-   Run `npm install` in directory from command prompt or IDE terminal.

-- **How to use** --

-   Run `npm start` in directory from command prompt or IDE terminal.

-	An interactive table will be rendered. Past launches are shown first by default.

-	To switch between past and upcoming launches, click on the toggle-button to the top right corner of the page.

-	To filter the entries in the table, simply type in your search parameter in the filter-input, left of the toggle button at the top of the page. The filter parameter goes through any of the data types shown.

-	Each entry in the table on the past launches-category is clickable with a link, opening a wikipedia-page where the user can read further on respective project.

-- **Developer's Notes** --

-	This application was developed using JavaScript-based front end framework ReactJS along with JavaScript library Redux. Front end library Semantic UI was used to create an effective UI format for the application. HTTP-client Axios was used to help implementing the SpaceX API.

-	Much of the code and file structure was inspired from examples out of the uDemy online course 'Modern React with Redux' by Stephen Grider, with a few differences.

-	Redux and get-requests where implemented to the project prior to any knowledge of Redux-thunk, Redux-form or other middleware, of which would have helped simplify the process and reduced the amount of code.