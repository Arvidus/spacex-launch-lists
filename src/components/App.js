import React from 'react';

import NavBar from './NavBarComponent/NavBar';
import Content from './ContentComponent/Content';

const App = () => {

    return (
        <div style={{paddingTop: '14px'}} className="ui container">
            <NavBar/>
            <Content/>
        </div>
    );
};

export default App;