import React from 'react';

import './SwitchStyle.css';

const Switch = ({ isOn, handleToggle, periodValue, textStyle }) => {
    return (
        <div className="middle aligned content switchDiv">
            <input
                checked={isOn}
                onChange={handleToggle}
                className="reactSwitchCheckbox"
                id={`reactSwitchNew`}
                type="checkbox"
            />
            <label
                className="reactSwitchLabel"
                htmlFor={`reactSwitchNew`}
                style={{ background: isOn && '#005288' }}
            >
                <h4 style={textStyle} className="periodName">{periodValue}</h4>
                <span className={`reactSwitchButton`}>
                </span>
            </label>
        </div>
    );
};

export default Switch;