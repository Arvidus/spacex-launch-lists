import React, { useState } from 'react';
import { connect } from 'react-redux';

import SearchInput from './SearchInputComponent/SearchInput';
import Switch from './SwitchComponent/Switch';

import { selectLaunches } from '../../redux/actions/ActionIndex';

import './NavBarStyle.css';

const NavBar = (props) => {

    const [period, setPeriod] = useState(false);
    let periodValue = "Past";
    let textStyle = {
        color: '#005288',
        right: '15%'
    };

    if (period) {
        periodValue = "Future";
        textStyle = {
            color: '#fff',
            left: '15%'
        };
        props.selectLaunches(props.launches.futureLaunches);
    } else {
        if (props.launches.pastLaunches){
            props.selectLaunches(props.launches.pastLaunches);
        }
    }

    return (
        <div className="ui menu stackable container">
            <div className="logo header">
                <img alt="SpaceX Logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/SpaceX-Logo.svg/500px-SpaceX-Logo.svg.png" />
            </div>
            <SearchInput/>
            <div style={{margin: 0}} className="header item right switchDiv">
                <Switch
                    isOn={period}
                    handleToggle={() => setPeriod(!period)}
                    periodValue={periodValue}
                    textStyle={textStyle}
                />
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {

    // console.log(state);

    return {
        launches: state.launches,
        selectedLaunches: state.selectedLaunches
    }
};

export default connect(mapStateToProps, {
    selectLaunches
})(NavBar);
