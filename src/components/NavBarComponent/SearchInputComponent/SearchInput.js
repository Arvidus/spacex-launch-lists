import React from 'react';
import { connect } from 'react-redux';

import {inputChange} from '../../../redux/actions/ActionIndex';

import './SearchInputStyle.css';

const SearchInput = (props) => {

    const onInput = (e) => {
        props.inputChange(e.target.value);
    };

    return (
        <div className="ui input right item">
           <input className="ui right floated" placeholder="Filter..." type="text" value={props.searchValue} onChange={onInput}/>
        </div>
    );
};

const mapStateToProps = (state) => {

    return {
        searchValue: state.searchValue
    }
};

export default connect(mapStateToProps, {
    inputChange
})(SearchInput);
