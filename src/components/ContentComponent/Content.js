import React from 'react';
import {connect} from 'react-redux';

import spaceX from "../../api/spaceX";

import {getFutureLaunches, getPastLaunches, selectLaunches} from '../../redux/actions/ActionIndex';

import './ContentStyle.css';

class Content extends React.Component {
    async getData() {
        let response =
            await spaceX.get("v3/launches/past",
                {headers: {'Content-Type': 'application/json'}}
            );
        await this.props.getPastLaunches(response.data);
        await this.props.selectLaunches(response.data);

        response =
            await spaceX.get("v3/launches/upcoming",
                {headers: {'Content-Type': 'application/json'}}
            );
        await this.props.getFutureLaunches(response.data);
    }

    componentDidMount() {
        this.getData();
    }

    handleRowClick = (link) => {
        if (link !== null) {
            window.open(link, "_blank");
        }
    };

    renderLaunches() {
        console.log(this.props.selectLaunches);
        return this.props.selectedLaunches
            .filter(launch => this.props.searchValue === ''
                || launch.mission_name.toLowerCase().includes(this.props.searchValue.toLowerCase())
                || new Intl.DateTimeFormat('en-GB', {
                    year: 'numeric',
                    month: 'numeric',
                    day: 'numeric'
                }).format(launch.launch_date_unix * 1000).includes(this.props.searchValue)
                || launch.launch_site.site_name.toLowerCase().includes(this.props.searchValue.toLowerCase())
                || launch.rocket.rocket_name.toLowerCase().includes(this.props.searchValue.toLowerCase()))
            .slice(0).reverse().map((launch) => {
                return (
                    <tr className={launch.links.wikipedia ? "clickable" : ""} key={launch.mission_name}
                        onClick={launch.links.wikipedia ? () => this.handleRowClick(launch.links.wikipedia) : ""}>
                        <td className="badge">
                            {launch.links.mission_patch_small ?
                                <img alt="Mission Badge" style={{maxHeight: "100px", width: "auto"}}
                                     src={launch.links.mission_patch_small}/> : ""}
                        </td>
                        <td>
                            <h3>{launch.mission_name}</h3>
                        </td>
                        <td>
                            {launch.launch_site.site_name}
                        </td>
                        <td>
                            {new Intl.DateTimeFormat('en-GB', {
                                year: 'numeric',
                                month: 'numeric',
                                day: 'numeric'
                            }).format(launch.launch_date_unix * 1000)}
                        </td>
                        <td>
                            {launch.rocket.rocket_name}
                        </td>
                    </tr>
                );
            })
    }


    render() {

        return (
            <div className="ui column eight wide">
                <table className="ui striped table stackable">
                    <thead>
                    <tr>
                        <th id={"missionBadge"}>Mission Badge</th>
                        <th>Mission Name</th>
                        <th>Launch Site</th>
                        <th>Launch Date</th>
                        <th>Rocket Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.renderLaunches()}
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state) => {

    //console.log(state);

    return {
        launches: state.launches,
        selectedLaunches: state.selectedLaunches,
        searchValue: state.searchValue
    }
};

export default connect(mapStateToProps, {
    getPastLaunches,
    getFutureLaunches,
    selectLaunches
})(Content);
