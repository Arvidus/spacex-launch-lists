import { combineReducers } from 'redux';

const launchReducer = (launches = {}, action) => {
    if (action.type === 'GET_PAST_LAUNCHES') {
        launches.pastLaunches = action.payload;
    } else if (action.type === 'GET_FUTURE_LAUNCHES') {
        launches.futureLaunches = action.payload;
    }
    
    return launches
};

const selectedLaunchesReducer = (selectedLaunches = [], action) => {
    if (action.type === 'SELECT_LAUNCHES'){
        selectedLaunches = action.payload;
    }
        return selectedLaunches
};

const searchValueReducer = (searchValue = "", action) => {
    if (action.type === 'INPUT_CHANGE'){
        searchValue = action.payload;
    }

    return searchValue
};

export default combineReducers({
    launches: launchReducer,
    selectedLaunches: selectedLaunchesReducer,
    searchValue: searchValueReducer
});