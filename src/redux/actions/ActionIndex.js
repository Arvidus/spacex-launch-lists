
export const getPastLaunches = (pastLaunches) => {
    return {
        type: 'GET_PAST_LAUNCHES',
        payload: pastLaunches
    }
};

export const getFutureLaunches = (futureLaunches) => {
    return {
        type: 'GET_FUTURE_LAUNCHES',
        payload: futureLaunches
    }
};

export const selectLaunches = (launches) => {
    console.log(launches);
    return {
        type: 'SELECT_LAUNCHES',
        payload: launches
    }
};

export const inputChange = (searchValue) => {
    return {
        type: 'INPUT_CHANGE',
        payload: searchValue
    }
};
